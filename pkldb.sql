/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.37-MariaDB : Database - pkl
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pkl` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pkl`;

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `admin` */

insert  into `admin`(`id`,`username`,`password`,`nama`) values (1,'sindi123','$2y$10$CP5C5bkPkGgSMYEtiSUWU.rIys651.caZkyMAsnyBzHxc0V2./3lC','Sindi Nur Kharisma'),(3,'indah123','$2y$10$smclEFa0faXToP4I1zV2LOB1GFktbyp8pUn709wb/95rMvOAsKnPW','Indah Wulansari'),(5,'admin123','$2y$10$Jw6blYdT43wlcIfOBNm4X.HcQhgdGUtPDCLzMdFEDt1KZNAIZnzd2','admin'),(6,'riska123','$2y$10$nZwJghM9xFPSoUNrxQA2su1jp1ndUPsflb1AEpOO39bicXrl.CeiO','Riska NA');

/*Table structure for table `industri` */

DROP TABLE IF EXISTS `industri`;

CREATE TABLE `industri` (
  `nib` varchar(30) NOT NULL,
  `namaind` varchar(50) DEFAULT NULL,
  `namaproduk` varchar(50) DEFAULT NULL,
  `tahun` varchar(10) DEFAULT NULL,
  `pendthn` varchar(30) DEFAULT NULL,
  `inthn` varchar(30) DEFAULT NULL,
  `pekerja` varchar(30) DEFAULT NULL,
  `alamatind` varchar(50) DEFAULT NULL,
  `id_kec` int(3) DEFAULT NULL,
  PRIMARY KEY (`nib`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `industri` */

insert  into `industri`(`nib`,`namaind`,`namaproduk`,`tahun`,`pendthn`,`inthn`,`pekerja`,`alamatind`,`id_kec`) values ('1232123456','graha mitra','kamera','2010','Kecil','1.000.000.000 - 15.000.000.000','>=20','kediri',11),('2','graha mitra','tape ketan','2012','Menengah','15.000.000.000','>=20','kediri',21);

/*Table structure for table `kecamatan` */

DROP TABLE IF EXISTS `kecamatan`;

CREATE TABLE `kecamatan` (
  `id_kec` int(3) NOT NULL AUTO_INCREMENT,
  `namakec` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_kec`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `kecamatan` */

insert  into `kecamatan`(`id_kec`,`namakec`) values (1,'Pagu'),(2,'Kunjang'),(3,'Purwoasri'),(4,'Plemahan'),(5,'Kayen Kidul'),(6,'Papar'),(7,'Ngasem'),(8,'Sampengrejo'),(9,'Semen'),(10,'Tarokan'),(11,'Mojo'),(12,'Grogol'),(13,'Banyakan'),(14,'Kandat'),(15,'Ngadiluwih'),(16,'Ngancar'),(17,'Ringinrejo'),(18,'Wates'),(19,'Gurah'),(20,'Kandangan'),(21,'Badas'),(22,'Pare'),(23,'Plosoklaten'),(24,'Puncu'),(25,'Kepung'),(26,'Kras');

/*Table structure for table `pemilik` */

DROP TABLE IF EXISTS `pemilik`;

CREATE TABLE `pemilik` (
  `nik` int(20) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `sosmed` varchar(20) DEFAULT NULL,
  `telp` varchar(20) DEFAULT NULL,
  `nib` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemilik` */

insert  into `pemilik`(`nik`,`nama`,`email`,`alamat`,`sosmed`,`telp`,`nib`) values (765431,'Indah Wulansari','kab.kediri@user.com','Bandar Lor','fb','089765432123','1232123456'),(32313121,'Yolanda Diah P','indahwulansariyfs@facebook.com','jongbiru','fb','98765432','2'),(2147483647,'Faris','Bandar Lor','ghgh@GMAIL.COM','fb','089765432123','1');

/*Table structure for table `skala` */

DROP TABLE IF EXISTS `skala`;

CREATE TABLE `skala` (
  `id_skala` int(11) NOT NULL AUTO_INCREMENT,
  `skala` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_skala`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `skala` */

insert  into `skala`(`id_skala`,`skala`) values (1,'Mikro'),(2,'Kecil'),(3,'Menengah'),(4,'Besar');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
