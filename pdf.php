<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<title>CETAK DATA INDUSTRI KABUPATEN KEDIRI</title>
</head>
<body>
 
	<center>
 
		<h2 style="padding-bottom:40px; padding-top:20px;">DATA INDUSTRI KABUPATEN KEDIRI</h2>
	</center>
	<?php 
include 'koneksi.php';
$db = new database();
?>
 	<table id="listdata" class="table table-striped table-bordered" style="color: black;">
					<thead class="thead">
						<tr>
							<th scope="col">NIB</th>
							<th scope="col">Nama Pemilik</th>
							<th scope="col">Nama Industri</th>
							<th scope="col">Nama Produk</th>
							<th scope="col">Tahun</th>
							<th scope="col">Skala Investasi</th>
							<th scope="col">Tenaga Kerja</th>
							<th scope="col">Alamat Industri</th>
							<th scope="col">Sosial Media</th>
							<th scope="col">Telepon</th>
							<th scope="col">Kecamatan</th>
							
						</tr>
					</thead>

					<tbody>
						<?php
						foreach ($db->tampil_data(['data']) as $data) { ?>
							<tr>
								<td> <?php echo $data['nib'];?> </td>
								<td> <?php echo $data['nama'];?> </td>
								<td> <?php echo $data['namaind'];?> </td>
								<td> <?php echo $data['namaproduk'];?> </td>
								<td> <?php echo $data['tahun'];?> </td>
								<td> <?php 
								if ($data['pekerja'] == '>=20' && $data['inthn']== '15.000.000.000' ) {
									echo "Besar";
								} else if ($data['pekerja'] == '>=20' && $data['inthn'] == '1.000.000.000 - 15.000.000.000' ){
									echo "Menengah";
								}else if ($data['pekerja'] == '>=20' && $data['inthn'] == '1.000.000.000' ){
									echo "Menengah";
								}else if ($data['pekerja'] == '1-19' && $data['inthn'] == '15.000.000.000' ) {
									echo "Menengah";
								} else if ($data['pekerja'] == '1-19' && $data['inthn'] == '1.000.000.000 - 15.000.000.000' ){
									echo "Menengah";
								}else if ($data['pekerja'] == '1-19' && $data['inthn'] == '1.000.000.000' ){
									echo "Kecil";
								}
								?> </td>
								<td> <?php echo $data['pekerja'];?> </td>
								<td> <?php echo $data['alamatind'];?> </td>
								<td> <?php echo $data['sosmed'];?> </td>
								<td> <?php echo $data['telp'];?> </td>
								<td> <?php echo $data['namakec'];?> </td>
								
								</tr>
							
						<?php } ?>
					</tbody>
				</table>
				
 
	<script>
		window.print();
	</script>
 
</body>
</html>