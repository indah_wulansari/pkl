<?php
$db = new database();
?>
<!doctype html>
<html>
<head>
<title>Data Industri Kabupaten Kediri</title>
</head>

<body>
	<header>
		<h3>DATA INDUSTRI KABUPATEN KEDIRI</h3>
	</header>
    <table class="table table-striped" >
					<thead class="thead">
						<tr>
							<th scope="col">NIB</th>
							<th scope="col">Nama Pemilik</th>
							<th scope="col">Nama Industri</th>
							<th scope="col">Nama Produk</th>
							<th scope="col">Tahun</th>
							<th scope="col">Skala Investasi</th>
							<th scope="col">Tenaga Kerja</th>
							<th scope="col">Alamat Industri</th>
							<th scope="col">Sosial Media</th>
							<th scope="col">Telepon</th>
							<th scope="col">Kecamatan</th>
						</tr>
					</thead>

					<tbody>
						
					<?php
						foreach ($db->tampil_data(['data']) as $data1) { ?>
							<tr>
								<td> <?php echo $data1['nib'];?> </td>
								<td> <?php echo $data1['nama'];?> </td>
								<td> <?php echo $data1['namaind'];?> </td>
								<td> <?php echo $data1['namaproduk'];?> </td>
								<td> <?php echo $data1['tahun'];?> </td>
								<td> <?php 
					
					if ($data1['pekerja'] == '>=20' && $data1['inthn']== '15.000.000.000' ) {
									echo "Besar";
								} else if ($data1['pekerja'] == '>=20' && $data1['inthn'] == '1.000.000.000 - 15.000.000.000' ){
									echo "Menengah";
								}else if ($data1['pekerja'] == '>=20' && $data1['inthn'] == '1.000.000.000' ){
									echo "Menengah";
								}else if ($data1['pekerja'] == '1-19' && $data1['inthn'] == '15.000.000.000' ) {
									echo "Menengah";
								} else if ($data1['pekerja'] == '1-19' && $data1['inthn'] == '1.000.000.000 - 15.000.000.000' ){
									echo "Menengah";
								}else if ($data1['pekerja'] == '1-19' && $data1['inthn'] == '1.000.000.000' ){
									echo "Kecil";
								}
								?> </td>
								<td> <?php echo $data1['pekerja'];?> </td>
								<td> <?php echo $data1['alamatind'];?> </td>
								<td> <?php echo $data1['sosmed'];?> </td>
								<td> <?php echo $data1['telp'];?> </td>
								<td> <?php echo $data1['namakec'];?> </td>
								</tr>
							</td>
						<?php } ?>
					</tbody>
				</table>
</body>
</html>